from django.shortcuts import render

# Create your views here.
def index(request):
    response = {'author' : 'Randa Alfarisyi'}
    html = 'lab_6/lab_6.html'
    return render(request, html, response)
